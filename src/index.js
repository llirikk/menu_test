import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { getToken } from './utils';
import "gestalt/dist/gestalt.css";
import { Link } from "react-router-dom";

//import Navbar from "./components/Navbar";
import Signin from "./components/Signin";
import Signup from "./components/Signup";
import Checkout from "./components/Checkout";
import Artworks from "./components/Artworks";
import Shop from "./components/Shop";

import registerServiceWorker from "./registerServiceWorker";

import "./components/App.css";
import Menu from "./components/Menu";
import YoutubeBackground from "react-youtube-background";
import ReactRevealText from "react-reveal-text";
import Slider from "react-slick";
import './components/slider/slider.css';

import Basic from './components/shadertoy/Basic'
import deviceDetect from "device-detect";
//import { Scrollbars } from 'react-custom-scrollbars';

const { device } = deviceDetect();
let linkType;

if (["iPad", "iPod", "iPhone", "Blackberry"].indexOf(device) !== -1) {
  linkType = "ios";
} else if (["WindowsMobile", "Android"].indexOf(device) !== -1) {
  linkType = "android";
} else {
  linkType = "desktop";
}

let artworks = {

  "vs1":{
    "id":1,
    "name": "Клетчатка – это сложный углевод, из которого состоят пустотелые волокна, образующие клеточные оболочки практически всех растений. Этот полисахарид не переваривается в желудке и тонком кишечнике человека, но выполняет ряд жизненно важных для здоровья функций.",
    "materials": "",
    "size": "",
    "year": '',
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1597370917/ration_pi/kletchatka_ljj1bq.png"
  },
  "vs2":{
    "id":2,
    "name": "Arches",
    "materials": "oil on board",
    "size": "25x75 cm",
    "year": 2017,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/%D0%90%D1%80%D0%BA%D0%B8_25%D0%A575_%D0%94%D0%92%D0%9F_%D0%B0%D0%BA%D1%80%D0%B8%D0%BB_rmgc8x.jpg"
  },
  "vs3":{
    "id":3,
    "name": "Spring",
    "materials": "oil on board",
    "size": "60x60 cm",
    "year": 2017,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/spring_60X60_canvas_acrylic_v2_ekhlpr.jpg"
  },
  "vs4":{
    "id":4,
    "name": "Between lines",
    "materials": "oil on canvas",
    "size": "80x80 cm",
    "year": 2016,
    "url": "https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/%D0%9C%D0%B5%D0%B6%D0%B4%D1%83_%D0%BB%D0%B8%D0%BD%D0%B8%D1%8F%D0%BC%D0%B8_80%D0%A580_%D1%85%D0%BE%D0%BB%D1%81%D1%82_%D0%BC%D0%B0%D1%81%D0%BB%D0%BE_v2_sfl6gl.jpg"      
  },
  "vs5":{
    "id":5,
    "name": "Fog",
    "materials": "oil on board",
    "size": "92x64 cm",
    "year": 2017,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/fog_fiberbord_oil_92X64_sm7a1z.jpg"
  },
  "vs6":{
    "id":6,
    "name": "Lines",
    "materials": "acrylic on canvas",
    "size": "95x95 cm",
    "year": 2017,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/lines_canvas_acrylic_95X95_t326cy.jpg"
  },
  "vs7":{
    "id":7,
    "name": "Splash",
    "materials": "acrylic on canvas",
    "size": "120x70 cm",
    "year": 2018,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/splash_canvas_acrylic_120X70_cfp1sb.jpg"
  },
  "vs8":{
    "id":8,
    "name": "Awaiting",
    "materials": "oil on canvas",
    "size": "95x90 cm",
    "year": 2018,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573650565/vs_artworks/awaiting_canvas_oil_95X90_x1t09w.jpg"
  },
  "vs9":{
    "id":9,
    "name": "Quietly",
    "materials": "oil on canvas",
    "size": "80x70 cm",
    "year": 2019,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573653309/vs_artworks/%D0%A2%D0%B8%D1%85%D0%BE_%D1%85%D0%BE%D0%BB%D1%81%D1%82_%D0%BC%D0%B0%D1%81%D0%BB%D0%BE_80%D0%A570_2019_fm2baa.jpg"
  },
  "vs10":{
    "id":10,
    "name": "Catch me",
    "materials": "acrylic on canvas",
    "size": "120x70 cm",
    "year": 2016,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573663370/vs_artworks/%D0%9F%D0%BE%D0%B9%D0%BC%D0%B0%D0%B9_%D0%BC%D0%B5%D0%BD%D1%8F_120%D0%A570_%D1%85%D0%BE%D0%BB%D1%81%D1%82_%D0%B0%D0%BA%D1%80%D0%B8%D0%BB_2016_had8m1.jpg"
  },
  "vs11":{
    "id":11,
    "name": "Organica",
    "materials": "acrylic on canvas",
    "size": "110x70 cm",
    "year": 2014,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573663575/vs_artworks/%D0%9E%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%BA%D0%B0_2014_11%D1%8570_a4aszq.jpg"
  },
  "vs12":{
    "id":12,
    "name": "Vitland",
    "materials": "acrylic on canvas",
    "size": "60x40 cm",
    "year": 2019,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573663962/vs_artworks/%D0%92%D0%B8%D1%82%D0%BB%D0%B0%D0%BD%D0%B4_%D1%85%D0%BE%D0%BB%D1%81%D1%82_%D0%B0%D0%BA%D1%80%D0%B8%D0%BB_60%D1%8540_2019_r1wgot.jpg"
  },
  "vs13":{
    "id":13,
    "name": "Yellow in summer",
    "materials": "acrylic on canvas",
    "size": "90x70 cm",
    "year": 2019,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573664063/vs_artworks/%D0%96%D0%B5%D0%BB%D1%82%D1%8B%D0%B9_%D0%BB%D0%B5%D1%82%D0%BE%D0%BC_90x70_%D0%B0%D0%BA%D1%80%D0%B8%D0%BB_%D0%BD%D0%B0_%D1%85%D0%BE%D0%BB%D1%81%D1%82%D0%B5_2019_ugnzcp.jpg"
  },
  "vs14":{
    "id":14,
    "name": "Navigation",
    "materials": "oil on board",
    "size": "90x60 cm",
    "year": 2017,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573664192/vs_artworks/%D0%9D%D0%B0%D0%B2%D0%B8%D0%B3%D0%B0%D1%86%D0%B8%D1%8F_%D0%BE%D1%80%D0%B3%D0%B0%D0%BB%D0%B8%D1%82_%D0%BC%D0%B0%D1%81%D0%BB%D0%BE_90%D1%8560_2017_nxhhm8.jpg"
  },
  "vs15":{
    "id":15,
    "name": "Sand",
    "materials": "acrylic on canvas",
    "size": "70x120 cm",
    "year": 2019,
    "url":"https://res.cloudinary.com/dahnjhzzu/image/upload/v1573664836/vs_artworks/%D0%9F%D0%B5%D1%81%D0%BE%D0%BA_%D1%85%D0%BE%D0%BB%D1%81%D1%82_%D0%B0%D0%BA%D1%80%D0%B8%D0%BB_120%D1%8570_15000_d6qana.jpg"
  },

};

class Paintings extends React.Component {

  render() {
    const Data = Object.values(artworks).map(p => {
      return (
        <div key={p.id}>
          <img src={p.url} />
          <div className='artwork_title'>
            <h4>{p.name}</h4>
            <div>{p.materials}</div>
            <div>{p.size}</div>
            <div>{p.year}</div>
          </div>
        </div>
      );
    })

    var settings = {
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    var divStyle = {
      height: "100vh",
      backgroundSize: 'cover',
      backgroundImage: `url("https://res.cloudinary.com/dahnjhzzu/image/upload/v1573483049/vs_artworks/vs_back_peyg0h.jpg")`
    };

    var history = require('browser-history')

    return (
      <div>
        <div onClick={() => {history(-1)}} className="buttonBack"><i className="fas fa-chevron-left"></i></div>
        <div>
          {linkType == "desktop" ? (
            <Basic /> 
          ) : (
            <div style={divStyle}></div>
          )}
        </div>         
        
      <div className="container">
        <Slider {...settings}>
          {Data}
        </Slider>
      </div>
      </div>
      );
    }
  }

const Main = () => {

  var divStyle = {
    height: "100vh",
    backgroundSize: 'cover',
    backgroundImage: `url("https://res.cloudinary.com/dahnjhzzu/image/upload/v1573483049/vs_artworks/vs_back_peyg0h.jpg")`
  };
  
  return (
    <div>
      <div className="artist_name">
      <Link to="/paintings" className="link-title">КОНСТРУКТОР ПИТАНИЯ</Link>
      </div> 
        <Menu />
        <div>
          {linkType == "desktop" ? (
            <Basic /> 
          ) : (
            <div style={divStyle}></div>
          )}
        </div>    
    </div>
  );
}

{/* <Scrollbars
		autoHide
		autoHeight
		autoHeightMax={'100%'}>

</Scrollbars> */}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    getToken() !== null ?
      <Component {...props} /> : <Redirect to={{
        pathname: '/signin',
        state: { from: props.location }
      }} />
  )} />
)

const Root = () => (
  <Router>
    {/* <React.Fragment> */}
      {/* <Navbar /> */}
      <Switch>
        <Route component={Main} exact path="/" />
        <Route component={Paintings} exact path="/paintings" />
        <Route component={Shop} exact path="/shop" />
        <Route component={Signin} path="/signin" />
        <Route component={Signup} path="/signup" />
        <PrivateRoute component={Checkout} path="/checkout" />
        <Route component={Artworks} exact path="/:brandId" />
      </Switch>
    {/* </React.Frakgment> */}
  </Router>
);

ReactDOM.render(<Root />, document.getElementById("root"));
registerServiceWorker();

if (module.hot) {
  module.hot.accept();
}
