import React from 'react';
import './Menu.css'
import { NavLink } from "react-router-dom";

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: true};

  }

  toggleMenu = () => {
  	this.setState({ open: !this.state.open });
  }
  
  render() {
  	const linksArray = [
    	{ name: "about", url: "#" },
      { name: "paintings", url: "#" },
      { name: "shop", url: "#" },
      { name: "contact", url: "#" }
    ];
    const socialArray = [
    	{
        icon: "fa fa-facebook-square", 
        url: "https://www.facebook.com/shvoren/"
      },{
        icon: "fa fa-instagram", 
        url: "https://www.instagram.com/viktoria_shvoren/"
      },{
        icon: "fa fa-vk", 
        url: "https://vk.com/id7010825"
      }
    ];
    
  	return (
      <div className="main_back">
       	<Panel 
          open={this.state.open} 
          links={linksArray} 
          socialLinks={socialArray}
        />
        <Button 
          toggle={this.toggleMenu} 
          open={this.state.open} 
        />
      </div>
    );
  }
};

class Button extends React.Component{
	render() {
  	return (
    	<button 
        className={this.props.open 
          ? "menu-toggle close-button" 
          : "menu-toggle "}
        onClick={this.props.toggle}
      > 
      {/* <i>{'\U+03D6'}</i> */}
      <img src="https://img.icons8.com/material/24/000000/pi.png"/>
      </button>
    );
  }
};

class Panel extends React.Component{
	render() {
  	return (
    	 <div 
         className={this.props.open 
           ? "menu-wrapper menu-open" 
           : "menu-wrapper"}
       >
         <Links 
           links={this.props.links} 
           open={this.props.open} 
         />
         <Social 
           socialLinks={this.props.socialLinks} 
           open={this.props.open}
         />
       </div>
    );
  }
};

class Links extends React.Component{
	render() {   
    return (
   	  <div className={this.props.open 
          ? "links-wrapper"
          : "links-wrapper links-wrapper-closed"}
      > 
        <ul className="link-list">
          <li className="link">       
            <NavLink to="/paintings/">Рацион ПИ</NavLink>
          </li>
          <li className="link">       
          <NavLink to="/shop">Магазин</NavLink>
          </li>
        </ul>
      </div>
    );
  }
};

class Social extends React.Component{
	render() {
  	const socialLinks = this.props.socialLinks.map((link) => (
      <a href={link.url}>
        <i className= {link.icon} />
      </a>
    ));
    
  	return (
    	<div 
        className={this.props.open 
          ? "social-wrapper social-open"
          : "social-wrapper social-closed"}
      > {socialLinks}
      </div>
    );
  }
};

export default Menu;
