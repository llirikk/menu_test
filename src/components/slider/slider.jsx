import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import Slider from "react-slick";
import './slider.css';
import { Style } from "react-style-tag";
import Detail from '../description/index';
import { Link, animateScroll as scroll } from "react-scroll";
import Poster from '../poster/index';

/* Custom arrows */
class RightNavButton extends React.Component {
	render() {
	  return <button {...this.props}><i class="fas fa-chevron-right"></i></button>
	}
}
  
class LeftNavButton extends React.Component {
	render() {
	  return <button {...this.props}><i class="fas fa-chevron-left"></i></button>
	}
}

class Carousel extends Component {
	
	render() {
		const settings = {
	  		speed: 500,
			slidesToShow: 8,
			slidesToScroll: 3,
			centerMode: true,

			prevArrow: <LeftNavButton/>,
			nextArrow: <RightNavButton/>,
			responsive: [{
				breakpoint: 1900,
					settings: {
					slidesToShow: 8,
					slidesToScroll: 3}
				},{
				breakpoint: 1700,
					settings: {
					slidesToShow: 7,
					slidesToScroll: 1}
				},{
				breakpoint: 1460,
					settings: {
					slidesToShow: 6,
					slidesToScroll: 1}
				},{
				breakpoint: 1300,
					settings: {
					slidesToShow: 5,
					slidesToScroll: 1}
				},{
				breakpoint: 1070,
					settings: {
					slidesToShow: 4,
					slidesToScroll: 1}
				},{
				breakpoint: 864,
					settings: {
					slidesToShow: 3,
					slidesToScroll: 1}
				},{
				breakpoint: 650,
					settings: {
					slidesToShow: 2,
					slidesToScroll: 1}
				},{
				breakpoint: 450,
					settings: {
					slidesToShow: 1,
					slidesToScroll: 1}
				}
			]
		};

		/* Получаем данные из props */
		let nameCategory = this.props.name;
		let filmsID = this.props.data;
		let carouselID = this.props.ID;
		const carID = 'car' + this.props.ID; 

		/* Формируем афиши для показа */
		let Data = filmsID.map (p => {
			const pic = "images/big/" + p.id + ".jpg";
			const id = p.id;
			const css = this.state.css;
			const film = 's' + id;
			const item = 's' + this.state.id;

			let films = window.superstate.hdbaza['mh'+id];

			//onMouseOver={() => this.props.handleChange(carouselID)}
			return  <div id={film}>
						<div  className={css}>
							<Link onClick={() => {this.updateData(id, carouselID)}} to={carID}>
								<Poster sourse={pic}/>							
								<p className="movie-title">{films.title}</p>
								<p className="movie-info">{films.year} | IMDB: 9,5</p>
							</Link>
						</div>

						<Style>
							{` #${item} .active-poster img {
								filter: contrast(1.1);
								transform: scale(1.08);
								box-shadow: 0 0 23px #000;      
								cursor: pointer;
								z-index: 30;
							}`}
						</Style>	
					</div>
		 })

		return (
			<div id={carID} className="carousel">

				<h2 className="header_film"><NavLink to={this.props.path}>{nameCategory} <i class="fas fa-chevron-right"></i></NavLink></h2>
				<Slider {...settings}>
					{Data}
				</Slider>	

				<Detail ID={this.state.ID} id={this.state.id} display={this.state.display} />
			</div>
		);
	}
}

export default Carousel;